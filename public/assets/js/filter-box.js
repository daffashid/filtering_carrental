function createFilterBox() {
    craeteParentElement()

    let formGroup1 = document.getElementById("form-group1")
    let formGroup2 = document.getElementById("form-group2")
    let formGroup3 = document.getElementById("form-group3")
    let formGroup4 = document.getElementById("form-group4")
    let formGroup5 = document.getElementById("form-group5")

    let drivers = ["Pilih Tipe Driver", "Dengan Sopir", "Tanpa Sopir"]
    let options = []
    drivers.forEach((a, b) => {
        let option = document.createElement("option")
        if (b > 0) {
            option.value = a
        }
        option.text = a
        options.push(option)
    })

    // form-group 1
    
    let labelDriver = document.createElement("label");
    labelDriver.innerText = "Tipe Driver"

    let selectDriver = document.createElement("select");
    selectDriver.id = "select-driver";
    selectDriver.classList.add("form-control");
    formGroup1.append(labelDriver,selectDriver);
    selectDriver.append(...options);
    selectDriver.onfocus = showBackdrop;
    selectDriver.onblur = hideBackdrop;
    selectDriver.onchange = hideBackdrop;

    // form-group 2

    let labelDate = document.createElement("label");
    labelDate.innerText = "Tanggal";

    let inputDate = document.createElement("input");
    inputDate.id = "input-tanggal";
    inputDate.type = "date";
    inputDate.classList.add("form-control");
    inputDate.onfocus = showBackdrop;
    inputDate.onblur = hideBackdrop;

    formGroup2.append(labelDate,inputDate);
    // form-group 3

    let labelJemput = document.createElement("label");
    labelJemput.innerText = "Waktu Jemput/Ambil";

    let inputJemput = document.createElement("input");
    inputJemput.id = "input-jemput";
    inputJemput.type = "time";
    inputJemput.classList.add("form-control");
    inputJemput.onfocus = showBackdrop;
    inputJemput.onblur = hideBackdrop;

    
    formGroup3.append(labelJemput,inputJemput);
    // form-group 4


    let labelPenumpang = document.createElement("label");
    labelPenumpang.innerText = "Jumlah Penumpang";

    let inputGroupPenumpang = document.createElement("div");
    inputGroupPenumpang.classList.add("input-group")

    let inputPenumpang = document.createElement("input");
    inputPenumpang.id = "input-penumpang";
    inputPenumpang.type = "number";
    inputPenumpang.classList.add("form-control");
    inputPenumpang.placeholder = "Jumlah Penumpang";
    inputPenumpang.onfocus = showBackdrop;
    inputPenumpang.onblur = hideBackdrop;

    let iconUserContainer = document.createElement("div");
    iconUserContainer.classList.add("input-group-append");
    
    inputGroupPenumpang.append(inputPenumpang, iconUserContainer);

    let iconUserWrapper = document.createElement("div");
    iconUserWrapper.classList.add("input-group-text","bg-white");
    iconUserWrapper.style.padding = "10px";

    let iconUser = document.createElement("img");
    iconUser.src = "./images/img/user-icon.svg";
    iconUserWrapper.append(iconUser);
    
    iconUserContainer.append(iconUserWrapper);
    formGroup4.append(labelPenumpang,inputGroupPenumpang);

    // form-group 5

    let labelButton = document.createElement("label");
    labelButton.innerHTML = "&nbsp";

    let searchButton = document.createElement("button");
    searchButton.id = "button-cari";
    searchButton.type = "button";
    searchButton.classList.add("btn", "btn-success","col-sm-12");
    searchButton.innerText = "Cari Mobil";
    searchButton.onclick = onButtonClicked;

    
    formGroup5.append(labelButton,searchButton);

}

async function onSearch(props) {
  let listcar = document.getElementById("list-car");
  listcar.innerHTML = "";
  let cars = await Binar.listCars();
  let arrCars = [];

  for (let i = 0; i < cars.length; i++) {
    if (
      cars[i].capacity >= props.passenger &&
      cars[i].available == props.withDriver &&
      cars[i].availableAt <= props.datetime
    ) {
      arrCars.push(`
      <div class="col-lg-4 d-flex justify-content-center " style="margin-top: 30px
        ;">
            <div class="listcar-card card" style="width: 25rem;">
                <img src="${cars[i].image}" height="300px" class="card-img-top" alt="${cars[i].manufacture}">
                <div class="card-body" style="font-family: Helvetica;">
                    <p>${cars[i].manufacture} ${cars[i].model}</p>
                    <p> <b>Rp ${cars[i].rentPerDay} / hari</b></p>
                    <p>${cars[i].description}</p>
                    <p><img src="./images/img/fi_users.svg" width="16" height="16"> ${cars[i].capacity}</p>
                    <p><img src="./images/img/fi_settings.png" width="16" height="16"> ${cars[i].transmission}</p>
                  <p><img src="./images/img/fi_calendar.svg" width="16" height="16"> Tahun  ${cars[i].year}</p>
                  </div>
                  <div class="card-footer">
                  <button class="btn btn-success col-lg-12">Pilih Mobil</button>
                </div>
                  
            </div>
        </div>
    `);
    console.log(cars[i]);
    }
  }
  listcar.innerHTML += arrCars.join("");
}

createFilterBox()

